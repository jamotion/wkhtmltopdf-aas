FROM ubuntu:14.04
MAINTAINER Renzo Meister <rm@jamotion.ch>

RUN sed 's/main$/main universe/' -i /etc/apt/sources.list
RUN apt-get update
RUN apt-get upgrade -y

# Download and install wkhtmltopdf
RUN apt-get install -y build-essential xorg libssl-dev libxrender-dev wget gdebi python-pip

RUN pip install werkzeug executor gunicorn

COPY  ./wkhtmltox-0.12.1.deb /tmp/wkhtmltox-0.12.1.deb
ADD ./app.py /app.py

RUN gdebi --n /tmp/wkhtmltox-0.12.1.deb

EXPOSE 80

ENTRYPOINT ["usr/local/bin/gunicorn"]

CMD ["-b", "0.0.0.0:80", "--log-file", "-", "app:application"]